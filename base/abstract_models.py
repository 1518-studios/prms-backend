from datetime import datetime

from fastapi import HTTPException
from sqlalchemy import Boolean, Column, Integer, \
    DateTime, desc
from sqlalchemy.orm import class_mapper, ColumnProperty
from starlette.status import HTTP_400_BAD_REQUEST

from base.db import Base, SessionLocal
from base.utility import responses, schemas

session = SessionLocal


def putdelete_not_found():
    """
    Call this function, if you want to send object
    not found message.
    """
    sch = schemas.DataModelOut(
        id="",
        result=[],
        message="Record not found",
        statusCode=404
    )
    return responses.invalid_response(message="Record not found",
                                      status_code=404)


class BaseId(Base):
    __abstract__ = True
    # db = next(get_db())
    id = Column(
        Integer,
        name="id",
        primary_key=True,
        autoincrement=True,
        index=True)
    isActive = Column(Boolean, name="isactive", default=True)

    @classmethod
    def get_instance(cls, id, user=None, isactive=True):
        query = session.query(cls).filter(cls.id == id,
                                          cls.isActive)
        if user:
            if hasattr(cls, "companyId"):
                query = query.filter(cls.companyId == user.companyId)
        for row in query:
            return row

    @classmethod
    def search(cls, **kwargs):
        q = session.query(cls)
        for key, val in kwargs.items():
            q = q.filter(getattr(cls, key) == val)
        q = q.filter(cls.isActive == True)
        return [rec for rec in q]

    @classmethod
    def create(cls, schema, user=None):
        if isinstance(schema, dict):
            schema.pop("_sa_instance_state", None)
            obj = cls(**schema)
        else:
            obj = cls(**schema.dict())
        # if user:
        #     obj.createdBy = user.id
        #     if hasattr(cls, "companyId"):
        #         obj.companyId = user.companyId

        session.add(obj)
        cls.commit(obj)
        return obj

    @classmethod
    def delete(cls, id):
        obj = cls.get_instance(id)
        if not obj:
            return putdelete_not_found()
        session.delete(obj)
        try:
            session.commit()
        except Exception as err:
            session.rollback()
            err = "%s" % err
            return responses.invalid_response(message=err)

    @classmethod
    def delete_from_db(cls, id, user=None):
        obj = cls.get_instance(id=id, user=user)
        if not obj:
            return putdelete_not_found()
        session.delete(obj)
        try:
            session.commit()
        except Exception as err:
            session.rollback()
            err = "%s" % err
            return responses.invalid_response(message=err)

    @classmethod
    def commit(cls, obj):
        session.commit()
        session.refresh(obj)


class AbstractBaseModel(Base):
    sort = "-createdOn"
    __abstract__ = True
    id = Column(
        Integer,
        name="id",
        primary_key=True,
        autoincrement=True,
        index=True)
    createdOn = Column(
        DateTime,
        name="createdon",
        default=datetime.utcnow)

    isActive = Column(Boolean, name="isactive", default=True)
    createdBy = Column(Integer, name="createdby", nullable=True)

    @classmethod
    def add_orderby(cls, q, **kwargs1):
        """
        Method to provide order of the records getting from DB.
        """
        sort = kwargs1.get("sort", None)
        if not sort:
            sort = cls.sort if hasattr(cls, "sort") else None
        orderby = kwargs1.pop("sort", sort)
        error = HTTPException(
            status_code=HTTP_400_BAD_REQUEST,
            detail="%s column not find in the model" % orderby,
            headers={"WWW-Authenticate": "Bearer"},
        )

        if orderby:
            if orderby.startswith("-"):
                attrs = ["-%s" % i for i in dir(cls) if not i.startswith('_')]
                order_desc = True
            else:
                attrs = [i for i in dir(cls) if not i.startswith('_')]
                order_desc = False
            if order_desc:
                attr = getattr(cls, orderby[1:])
                attr = desc(attr)
            else:
                attr = getattr(cls, orderby)
            q = q.order_by(attr)
        else:
            q = q.order_by(desc(cls.createdOn))
        return q

    @classmethod
    def add_limit(cls, q, **kwargs1):
        """ This is to provide the pagination to the search """
        limit = kwargs1.pop('_limit', None)
        offset = kwargs1.pop("_offset", None)
        q = q.limit(limit).offset(offset)
        return q

    @classmethod
    def get_part_query(cls, query=None, **kwargs1):
        from_datetime = kwargs1.pop("fromDate", None)
        to_datetime = kwargs1.pop("toDate", None)
        createdby = kwargs1.pop("createdBy", None)
        updatedby = kwargs1.pop("updatedBy", None)
        name = kwargs1.pop("name", None)
        is_active = kwargs1.pop("isActive", None)
        request = kwargs1.pop("request", None)
        user = kwargs1.pop("user", None)
        if query:
            q = query
        else:
            q = session.query(cls)
        if from_datetime:
            q = q.filter(cls.createdOn >= from_datetime)
        if to_datetime:
            q = q.filter(cls.createdOn < to_datetime)
        if createdby:
            q = q.filter(cls.createdBy == createdby)
        if updatedby:
            q = q.filter(cls.updatedBy == updatedby)
        if name:
            names = name.split(",")
            q = q.filter(cls.name.in_(names))
        if is_active == True:
            q = q.filter(cls.isActive)
        elif is_active == False:
            q = q.filter(cls.isActive == None)
        elif is_active == "all":
            pass
        else:
            q = q.filter(cls.isActive)
        if user:
            if hasattr(cls, "companyId"):
                q = q.filter(cls.companyId == user.companyId)
        for key, val in kwargs1.items():
            if key in ["raw", "_sort", "sort", "_limit", "_offset"]:
                continue
            q = q.filter(getattr(cls, key) == val)

        return q

    @classmethod
    def get_query(cls, *args, **kwargs):
        kwargs1 = kwargs.copy()
        q = cls.get_part_query(**kwargs1)

        q = cls.add_orderby(q, **kwargs1)
        q = cls.add_limit(q, **kwargs1)
        return q

    @classmethod
    async def count(cls, query=None, *args, **kwargs):
        if not query:
            query = cls.get_query(*args, **kwargs)
        return query.count()

    @classmethod
    def get_cols(cls, not_required_cols=[]):
        properties = [col for col in class_mapper(cls).iterate_properties]
        # instrumented_relations = InstrumentedList
        relations = [col for col in properties if
                     not isinstance(col, ColumnProperty)]
        columns = [col for col in properties if not col in relations]
        columns = [col for col in columns if col.key not in not_required_cols]
        relations = [col for col in relations if
                     col.key not in not_required_cols]
        return columns, relations

    @classmethod
    def get_data(cls, q, not_required_cols=[]):
        not_required_cols.append("companyId")
        columns, relations = cls.get_cols(not_required_cols)
        records = []
        for record in q:
            # actual columns
            data_cols = {col.key: getattr(record, col.key) for col in columns}
            data_relations = {}
            # for backref relations
            for col in relations:
                key = col.key
                attr = getattr(record, col.key)
                if col.backref and attr:
                    data_relations[key] = attr.name
                else:
                    data_relations[key] = None
            # norn back ref relations
            # data_rev_relations = {col.key:getattr(record,col.key)
            #     for col in relations if not col.backref}
            # for col,values in data_rev_relations.items():
            #     data_relations[col]=[val.id for val in values]
            data_cols.update(data_relations)
            records.append(data_cols)
        return records

    @classmethod
    async def list(cls, *args, **kwargs):
        q = cls.get_query(*args, **kwargs)
        request = kwargs.pop("request")
        records = cls.get_data(q)
        return records
        # return q

    @classmethod
    def search(cls, *args, **kwargs):
        q = cls.get_query(*args, **kwargs)
        return q

    @classmethod
    def message(cls, obj, msg):
        table_name = cls.__tablename__

        return {
            "message": "%s %s successfully" %
                       (table_name.capitalize(),
                        msg),
            "id": obj.id,
            "statusCode": 200}

    @classmethod
    def raiseUnauthorizeException(cls, user):
        raise HTTPException(
            status_code=403,
            detail="%s Not have an access to do this job" % user.userName,
            headers={"WWW-Authenticate": "Bearer"},
        )

    @classmethod
    def save(cls, schema, user):
        obj = cls.create(schema, user)
        return cls.message(obj, "created")

    @classmethod
    def bulk_save_objects(cls, schemas, user):
        obj = []
        for schema in schemas:
            if isinstance(schema, dict):
                schema.pop("_sa_instance_state", None)
            else:
                schema = schema.dict()
            for key, value in schema.items():
                if value == "":
                    schema[key] = None
            if user:
                schema['createdBy'] = user.id
                if hasattr(cls, "companyId"):
                    schema['companyId'] = user.companyId
            obj.append(cls(**schema))
        session.add_all(obj)

    @classmethod
    def bulk_update(cls, data, user):
        session.bulk_update_mappings(cls, data)
        session.commit()

    @classmethod
    def create(cls, schema, user):
        if isinstance(schema, dict):
            schema.pop("_sa_instance_state", None)
        else:
            schema = schema.dict()
        for key, value in schema.items():
            if value == "":
                schema[key] = None

        obj = cls(**schema)
        if user:
            obj.createdBy = user.id
            if hasattr(cls, "companyId"):
                obj.companyId = user.companyId
        session.add(obj)
        cls.commit(obj)
        return obj

    @classmethod
    def commit(cls, obj):
        if obj not in session:
            session.add(obj)
        session.commit()
        session.refresh(obj)

    @classmethod
    def delete_commit(cls, obj):
        try:
            session.commit()
            session.refresh(obj)
        except Exception as err:
            session.rollback()
            error = "Can not delete record, can deactivate"
            return responses.invalid_response(message=error)

    @classmethod
    def get(cls, id, user, not_required_cols=[]):
        sch = schemas.GetDataModelOut()
        q = session.query(cls).filter(cls.id == id,
                                      cls.isActive)
        if user:
            if hasattr(cls, "companyId"):
                q = q.filter(cls.companyId == user.companyId)
        data = cls.get_data(q, not_required_cols)
        for row in data:
            sch.message = "Record found successfully"
            sch.result = [row]
            return sch.__dict__
        return responses.invalid_response_get()

    @classmethod
    def get_instance(cls, id, user, isactive=True):
        if isactive:
            query = session.query(cls).filter(cls.id == id, cls.isActive)
        else:
            query = session.query(cls).filter(cls.id == id)
        if user:
            if hasattr(cls, "companyId"):
                query = query.filter(cls.companyId == user.companyId)
        for row in query:
            return row

    @classmethod
    def _update(cls, id, schema, user):
        obj = cls.get_instance(id=id, user=user)
        if not obj:
            return putdelete_not_found()
        if isinstance(schema, dict):
            schema = cls(**schema)
        else:
            schema = schema.dict()
            schema.pop('_sa_instance_state', None)
            schema = cls(**schema)
        data = {
            key: value for key,
                           value in schema.__dict__.items() if
            value is not None}
        for key, value in data.items():
            if value in ["", 0]:
                data[key] = None
        data.pop("_sa_instance_state", None)
        if data:
            obj = session.query(cls).filter(cls.id == id).update({getattr(
                cls, key): value for key, value in data.items()},
                                                                 synchronize_session=False)
        obj = cls.get_instance(id=id, user=user)
        cls.commit(obj)
        return cls.message(obj, "updated")

    @classmethod
    def update(cls, id, schema, user):
        return cls._update(id, schema, user)

    @classmethod
    def delete(cls, id, user):
        obj = cls.get_instance(id=id, user=user)
        if not obj:
            return putdelete_not_found()
        obj.isActive = None
        cls.commit(obj)
        return cls.message(obj, "deleted")

    @classmethod
    def delete_from_db(cls, id, user):
        obj = cls.get_instance(id=id, user=user)
        if not obj:
            return putdelete_not_found()
        session.delete(obj)
        try:
            session.commit()
        except Exception as err:
            session.rollback()
            err = "%s" % err
            return responses.invalid_response(message=err)
        return cls.message(obj, "deleted")

    @classmethod
    def deactivate(cls, id, user):
        obj = cls.get_instance(id=id, user=user)
        if not obj:
            return putdelete_not_found()
        obj.isActive = None
        cls.commit(obj)
        return cls.message(obj, "deactivated")

    @classmethod
    def activate(cls, id, user):
        obj = cls.get_instance(id, isactive=False, user=user)
        if not obj:
            return putdelete_not_found()
        obj.isActive = True
        cls.commit(obj)
        return cls.message(obj, "activated")
