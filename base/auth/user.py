""" File to add some methods for user operations """
from typing import Tuple
import requests
from fastapi import HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette.authentication import AuthenticationBackend
from starlette.requests import HTTPConnection
from starlette.status import HTTP_401_UNAUTHORIZED

from base.db import session_scope
from base.models import User
from base.utility.constants import *
from base.utility.utilities import get_admin_headers

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/users/createtoken")


def get_login_user(request, username):
    """ method to get current user """
    credentials_exception = HTTPException(
        status_code=HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        user = get_wfm_user(username)
        if not user:
            raise credentials_exception
    except Exception as Err:
        raise credentials_exception
    return user


def get_wfm_user(email):
    url = f'{wfm_backend_host}/users/wfmcredential/'
    response = requests.post(url, json={"email": email},
                             headers=get_admin_headers(), verify=False)
    wfm_user = response.json()
    user = User.get_instance(id=1, user=None)
    user.companyId = wfm_user.get('companyId', None)
    user.userName = wfm_user.get("userName", None)
    user.isLevelThreeAdmin = wfm_user.get("isLevelThreeAdmin", None)
    user.name = ' '.join(filter(
        None, (wfm_user['firstName'], wfm_user['lastName'])))
    return user


class AuthBackend(AuthenticationBackend):
    async def authenticate(
            self, conn: HTTPConnection
    ) -> Tuple[bool]:
        username = conn.headers.get('x-user-id', None)
        token = conn.headers.get('authorization', None)
        path = conn.url.path
        error = None
        if path in TOKEN_NOT_REQUIRED or "/images/" in path:
            error = None
            status_code = 200
            return True, None
        else:
            if username:
                try:
                    with session_scope() as session:
                        conn.state.session = session
                        user = get_login_user(conn, username)
                    return True, user
                except Exception as err:
                    if 'detail' in dir(err):
                        error = err.detail
                    else:
                        error = err
                    status_code = err.status_code
            else:
                status_code = 400
                error = "Missing Token"
        if error:
            return False, None
