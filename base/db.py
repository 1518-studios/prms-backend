import os
from contextlib import contextmanager

from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, ModelConversionError
from sqlalchemy import create_engine, MetaData, event
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, mapper, scoped_session

engine = create_engine(
    URL(
        'mysql',
        username=os.environ.get("MYSQL_USER", "root"),
        password=os.environ.get("MYSQL_PASSWORD", "root"),
        host=os.environ.get("MYSQL_HOST", "localhost"),
        port=os.environ.get("MYSQL_PORT", "3306"),
        database=os.environ.get("MYSQL_DATABASE", "prms"),
    ),
    pool_pre_ping=True,
    pool_size=30,
    max_overflow=0

)

# SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
SessionLocal = scoped_session(
    sessionmaker(autocommit=False, autoflush=False, bind=engine,
                 expire_on_commit=False))
metadata = MetaData()
Base = declarative_base()


class Connection:
    _instance = None

    def __new__(cls, *args, **kwars):
        if cls._instance:
            return cls._instance
        cls._instance = SessionLocal()
        return cls._instance


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = SessionLocal()
    try:
        yield session
        session.commit()
    except Exception as err:
        session.rollback()
        raise err
    finally:
        session.close()


def get_db():
    # try:
    db = Connection()
    # Base.metadata.create_all(engine)
    yield db


# finally:
#     db.close()


def setup_schema(Base, session):
    def setup_schema_fn():
        for class_ in Base._decl_class_registry.values():
            if hasattr(class_, "__tablename__"):
                if class_.__name__.endswith("Schema"):
                    raise ModelConversionError(
                        "For safety, setup_schema can not be used when a"
                        "Model class ends with 'Schema'"
                    )

                class Meta(object):
                    model = class_
                    sqla_session = session

                schema_class_name = "%sSchema" % class_.__name__

                schema_class = type(schema_class_name, (SQLAlchemyAutoSchema,),
                                    {"Meta": Meta})

                setattr(class_, "__awl__", schema_class)

    return setup_schema_fn


if __name__ == "__main__":
    # Listen for the SQLAlchemy event and run setup_schema.
    # Note: This has to be done after Base and session are setup
    event.listen(mapper, "after_configured",
                 setup_schema(Base, next(get_db())))
    Base.metadata.create_all(engine)
