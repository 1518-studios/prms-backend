import logging
import os
import re

from fastapi import FastAPI
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import JSONResponse

from base.auth.user import AuthBackend
from base.db import engine, session_scope, Base
from base.utility.constants import ERROR_RESPONSE, ALLOWED_ORIGINS, \
    TOKEN_NOT_REQUIRED, ENVIRONMENTS
from base.utility.error_utilities import genErrorMessage
from base import urls as base_urls

app = FastAPI()
logger = logging.getLogger("gunicorn.error")
RUNNING_ENV = os.environ.get("ENVIRONMENT", None)
if RUNNING_ENV in ENVIRONMENTS:
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)
    path = f"/var/log/PRMS/{RUNNING_ENV}_log.log"
    logging.basicConfig(filename=path,
                        level=logging.DEBUG,
                        format='%(asctime)s %(levelname)s %(message)s'
                               '%(pathname)s Line: %(lineno)d')


@app.on_event("startup")
def startup_event():
    Base.metadata.create_all(bind=engine)
    app.add_middleware(AuthenticationMiddleware, backend=AuthBackend())


@app.middleware("http")
async def error_middleware(request: Request, call_next):
    error = None
    status_code = None
    response = None
    try:
        logger.info("%s" % request.url.path)
        if request.url.path in TOKEN_NOT_REQUIRED or request.user:
            with session_scope() as session:
                request.state.session = session
                flag = True
                if flag:
                    response = await call_next(request)
                    response_code = response.__dict__.get('status_code', None)
                else:
                    pass

        else:
            logger.info("Invalid user")
            error = "Invalid user"
            status_code = 403
    except Exception as err:
        # raise err
        # os.environ["request"] = None
        str_error = str(error)
        if len(err.args) == 2:
            status_code, error = err.args
        else:
            error = err
        logger.error("%s" % error)
        if not status_code:
            status_code = 422
        errors = re.findall(":  Key.*", str_error)
        if errors:
            error = errors[0]
            error = error.replace(":  Key", "Resource ")
    else:
        if response:
            status_code = response.status_code
            if status_code == 423:
                return response
            if 300 <= status_code < 500:
                error = "unknown"
                async for error1 in response.body_iterator:
                    error = genErrorMessage(error1)
                    break
                logger.error(error)
        # os.environ["request"] = None
    if error:
        error = str(error)
        dic = {}
        if "MySQL" not in error:
            if ":" in error:
                cv = error.split(":")
                dic[cv[0]] = cv[1]
            else:
                dic["msg"] = error
            if "," and ":" in error:
                dic = {}
                check = error.split(",")
                for i in check:
                    kvalue = i.split(":")
                    if len(kvalue) >= 2:
                        dic[kvalue[0]] = kvalue[1]
        else:
            if RUNNING_ENV in ENVIRONMENTS:
                error = "Oops something went wrong :( " \
                        "Please contact your administrator"
            dic["database"] = error
        if "extra fields" in error:
            error = "Extra fields not permitted"
        if "not exist" in error:
            error = error.replace(":", "")

        ERROR_RESPONSE.update(
            {"message": error, "error": dic, "statusCode": status_code or 200})
        response = JSONResponse(content=ERROR_RESPONSE,
                                status_code=status_code)
    return response

app.add_middleware(
    CORSMiddleware,
    allow_origins=ALLOWED_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    expose_headers=["*"],
)

app.include_router(base_urls.router)



