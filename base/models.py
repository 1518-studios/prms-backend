from base.abstract_models import AbstractBaseModel


class User(AbstractBaseModel):
    """This is model class for user"""
    __tablename__ = "user"
