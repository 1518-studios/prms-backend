import os

RUNNING_ENV = os.environ.get("ENVIRONMENT", None)
ENVIRONMENTS = ["PRODUCTION", "DEV", "QA", "BETA", "PRMSPYSQA", "PRMSPYUAT"]
wfm_backend_host = os.environ.get("WFM_BACKEND_HOST", "http://localhost:8001")
crm_backend_host = os.environ.get("CRM_BACKEND_HOST", "http://127.0.0.1:8000")
TOKEN_NOT_REQUIRED = ["/docs", "/openapi.json"]
ERROR_RESPONSE = {"message": None, "id": None, "result": []}
ALLOWED_ORIGINS = []
origins = os.environ.get("ALLOWED_ORIGINS", "")
if origins == "*":
    ALLOWED_ORIGINS = ["*"]
else:
    ALLOWED_ORIGINS = origins.split(",")
FILE_SIZE = 2
ACCESS_KEY = os.environ.get("ACCESS_KEY", "AKIAUIOKSOGZCJCRBQHP")
SECRET_KEY = os.environ.get("SECRET_KEY", "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")
BUCKET = os.environ.get("BUCKET", "ptw-dev-prms")
BUCKET_URL = os.environ.get("BUCKET_URL", None)

ALLOWED_EXTENSIONS_DICT = {'pdf': 'application/pdf', 'png': 'image/png',
                           'jpg': 'image/jpeg', 'jpeg': 'image/jpeg',
                           'gif': 'image/gif', 'doc': 'application/msword',
                           'docx': 'application/vnd.openxmlformats-'
                                   'officedocument.wordprocessingml.document',
                           'xlsx': 'application/vnd.openxmlformats-'
                                   'officedocument.spreadsheetml.sheet',
                           'txt': 'text/plain'}