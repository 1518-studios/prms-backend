""" File for error utilities """
import json


def genErrorMessage(s):
    """ Method to generate the error message from the """
    if not s:
        return "Check your URL ending with / or not"
    if isinstance(s, (str, bytes)):
        s = json.loads(s)
    detail = s.get('detail') or s.get("message")
    if isinstance(detail, list):
        err = ",".join(["%s:%s" % (k.get("loc")[-1], k.get("msg"))
                        for k in detail])
    else:
        err = detail
    return err
