from typing import List

from fastapi import HTTPException

from base.utility.schemas import DataModelOut, GetDataModelOut


def invalid_response(message="Permission Denied.", status_code=422):
    raise HTTPException(status_code, message)


def valid_response(message="Success", result=[], status_code=200, id=""):
    if not message:
        message = "Success"
    sch = DataModelOut(statusCode=status_code, message=message,
                       id=id, result=result)
    return sch.__dict__


def valid_response_get(
        count: int = 1,
        next: str = None,
        previous: str = None,
        result: List[dict] = [{}],
        message: str = "Records Found Successfully",
        id: str = None,
        statusCode: int = 200):
    return GetDataModelOut(count=count,
                           next=next,
                           previous=previous,
                           result=result,
                           message=message,
                           id=id,
                           statusCode=statusCode).__dict__


def invalid_response_get():
    raise HTTPException(404, "Record not found")
