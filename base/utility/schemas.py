from datetime import datetime
from typing import List

from pydantic import BaseModel


class GetDataModelOut(BaseModel):
    count: int = 1
    next: str = None
    previous: str = None
    result: List[dict] = [{}]
    message: str = "Records Found Successfully"
    id: str = None
    statusCode: int = 200


class DataModelOut(BaseModel):
    message: str = ""
    id: str = ""
    input_data: dict = None
    result: List[dict] = []
    statusCode: int


class AbstractBaseModelIn(BaseModel):
    class Config:
        extra = "forbid"
        orm_mode = True

    def cols_validate(self, request, instance_id=None):
        return True, None

    def validator(self, request, instance_id=None):
        if instance_id:
            valid_resp, error = self.cols_validate(request,
                                                   instance_id=instance_id)
        else:
            valid_resp, error = self.cols_validate(request)

        if not valid_resp:
            error_string = ""
            if isinstance(error, str):
                error_string = f"{error}, "
            elif isinstance(error, list):
                for error_obj in error:
                    if isinstance(error_obj, str):
                        error_string += f"{error_obj}, "
                    elif isinstance(error_obj, dict):
                        for key, val in error_obj.items():
                            error_string += f"{key.capitalize()}-{val}, "
            elif isinstance(error, dict):
                for key, val in error.items():
                    error_string += f"{key.capitalize()}-{val}, "
            if error_string:
                last_char_index = error_string.rfind(", ")
                error_string = f"{error_string[:last_char_index]}."
            raise ValueError(error_string)
        return True, None


class AbstractBaseModelOut(BaseModel):
    id: str
    name: str = None
    createdOn: datetime = None
    createdBy: str = None
    isActive: bool = True
    comments: str = None


class AbstractBaseModelUp(BaseModel):
    class Config:
        extra = "forbid"
        orm_mode = True

    def cols_validate(self, request, instance_id=None):
        return True, None

    def validator(self, request, instance_id=None):
        if instance_id:
            valid_resp, error = self.cols_validate(request,
                                                   instance_id=instance_id)
        else:
            valid_resp, error = self.cols_validate(request)
        if not valid_resp:
            error_string = ""
            if isinstance(error, str):
                error_string = f"{error}, "
            elif isinstance(error, list):
                for error_obj in error:
                    if isinstance(error_obj, str):
                        error_string += f"{error_obj}, "
                    elif isinstance(error_obj, dict):
                        for key, val in error_obj.items():
                            error_string += f"{key.capitalize()}-{val}, "
            elif isinstance(error, dict):
                for key, val in error.items():
                    error_string += f"{key.capitalize()}-{val}, "
            if error_string:
                last_char_index = error_string.rfind(", ")
                error_string = f"{error_string[:last_char_index]}."
            raise ValueError(error_string)
        return True, None
