from dateutil import parser
import magic
import mimetypes
import shutil
import os
import boto3
from datetime import datetime

from fastapi_contrib.pagination import Pagination as FastapiPagination
from passlib.context import CryptContext

from base.utility.schemas import DataModelOut
from base.utility import responses
from base.utility.constants import ALLOWED_EXTENSIONS_DICT, ACCESS_KEY, \
    SECRET_KEY, BUCKET, FILE_SIZE

PWDCONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")

host = "http://localhost:8001/"


def response(data):
    return {"count": len(data), "result": data}


def invalid_response(message="Not permitted"):
    sch = DataModelOut(statusCode=422, message=message,
                       id="", result=[])
    return sch.__dict__


def valid_response(message="Success", result=[], status_code=200, id=""):
    if not message:
        message = "Success"
    sch = DataModelOut(statusCode=status_code, message=message,
                       id=id, result=result)
    return sch.__dict__


class Pagination(FastapiPagination):
    default_offset = 0
    default_limit = 15
    max_offset = None
    max_limit = None


class QueryParams:
    def __init__(
            self,
            name: str = None,
            fromDate: datetime = None,
            toDate: datetime = None,
            createdBy: str = None,
            updatedBy: str = None,
            sort: str = None,
            isActive: bool = True,
            request=None):
        self.name = name
        self.fromDate = fromDate
        self.toDate = toDate
        self.createdBy = createdBy
        self.updatedBy = updatedBy
        self.sort = sort
        self.isActive = isActive
        # self.user = user
        self.request = request


class GetIdParam:
    def __init__(self, request=None):
        self.request = request


def pagination(query_params, pagination, serializer):
    if not isinstance(query_params, dict):
        query_params = query_params.__dict__
    filter_kwargs = {
        key: value for key, value in query_params.items() if value is not None}

    for key, value in filter_kwargs.items():
        if value == "":
            filter_kwargs[key] = None
    return pagination.paginate(serializer_class=serializer, **filter_kwargs)


def get_admin_headers():
    headers = {"x-user-id": "Admin"}
    return headers


def allowed_file(file):
    mime_type, encoding = mimetypes.guess_type(file.filename)
    if mime_type is None:
        mime_type = magic.from_buffer(file.file.read(), mime=True)
    file.file.seek(0)
    if mime_type not in ALLOWED_EXTENSIONS_DICT.values():
        return responses. \
            invalid_response(message="Malicious file: The file(s) you have "
                                     "uploaded may contain malicious data.")
    ext = file.filename.rsplit('.', 1)[1].lower()
    return ALLOWED_EXTENSIONS_DICT.get(ext, None) == mime_type and ext in \
           ALLOWED_EXTENSIONS_DICT


def split_file_name(filename):
    f = filename.rsplit('.', 1)
    return f[0].lower(), f[1].lower()


def upload_to_s3(file, filepath):
    file = file
    file_object = file.file
    filename, ext = split_file_name(file.filename)
    new_filename = filename + datetime.now().strftime(
        "%Y-%m-%d_%I-%M-%S_%p") + "." + ext
    if not os.path.exists("images"):
        os.makedirs("images")
    upload_file = open(os.path.join('images', new_filename), 'wb')
    shutil.copyfileobj(file_object, upload_file)
    upload_file.close()
    file_size = os.stat(upload_file.name).st_size
    if (file_size / (1024 * 1024)) > FILE_SIZE:
        return {"error": "%s attachment exceeds 2MB, please upload " \
                         "another file" % filename}
    try:
        s3 = boto3.client("s3", aws_access_key_id=ACCESS_KEY,
                          aws_secret_access_key=SECRET_KEY)
        s3.upload_file(os.path.join("images", new_filename), BUCKET,
                       f"{filepath}{new_filename}")
        os.remove(os.path.join('images', new_filename))
    except:
        return {"error": "Unable to upload. Please try again."}
    return {"filename": file.filename, "filepath": f"{filepath}{new_filename}"}


def get_required_time_format(time_str):
    """get current time with year from hh:mm am/pm time format"""
    current_timing = parser.parse(time_str).isoformat()
    time_obj = datetime.strptime(current_timing, '%Y-%m-%dT%H:%M:%S')
    return time_obj
